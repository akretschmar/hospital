
package hospital;

import java.util.List;


public class Patient {
    private String name;
    private String dateOfBirth;
    private String familyDoctor;
    private List<Wristband> wristbands;
    
    public Patient(String name, String dateOfBirth, String familyDoctor, List<Wristband> wristbands){
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.familyDoctor = familyDoctor;
        this.wristbands = wristbands;
        
    }
    
    public String getPatientName(){
        return this.name;
    }
    public String getPatientInfo(){
           return "Name: " +this.name + "\nDOB: " + this.dateOfBirth 
                + "\nFamily Dr: " + this.familyDoctor
                + "\nWristband: " + this.wristbands;
    }
    
}
