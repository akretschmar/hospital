
package hospital;
import java.util.List;
import java.util.ArrayList;

public class HospitalSimulation {
    public static void main(String[] args) {
        
        //Making Wristbands
        Wristband w1 = new Wristband("Ae011", "Penicillin Allergy");
        Wristband w2 = new Wristband("Ae012", "Zyrtec Allergy");
        Wristband w3 = new Wristband("Ae045", "Psoriasis");
        Wristband w4 = new Wristband("Ae046", "Vitiligo");
        Wristband w5 = new Wristband("Ae073", "Celiac");
        Wristband w6 = new Wristband("Ae074", "IBS");
        Wristband w7 = new Wristband("Ae083", "Diabetes II");
        
        //Patient 1's wristbands
        List<Wristband> p1wb = new ArrayList<>();
        p1wb.add(w1);
        p1wb.add(w6);
        
        //Patient 2's wristbands
        List<Wristband> p2wb = new ArrayList<>();
        p2wb.add(w1);
        
        //Patient 3's wristbands
        List<Wristband> p3wb = new ArrayList<>();
        p3wb.add(w7);
        
        //Patient 4's wristbands
        List<Wristband> p4wb = new ArrayList<>();
        p4wb.add(w3);
        
        //Patient 5's wristbands
        List<Wristband> p5wb = new ArrayList<>();
        p5wb.add(w4);
        
        //Patient 6's wristbands
        List<Wristband> p6wb = new ArrayList<>();
        p6wb.add(w7);
        p6wb.add(w6);  
        
        //Patient 7's wristbands
        List<Wristband> p7wb = new ArrayList<>();
        p7wb.add(w6); 
        
        //Patient 8's wristbands
        List<Wristband> p8wb = new ArrayList<>();
        p8wb.add(w5);
        
        //Patient 9's wristbands
        List<Wristband> p9wb = new ArrayList<>();
        p9wb.add(w2);
        
        //Patient 10's wristbands
        List<Wristband> p10wb = new ArrayList<>();
        p10wb.add(w1);
        p10wb.add(w2);
        
        //Creating 10 Patients
        Patient p1 = new Patient("Cindy Burns", "02/24/1980", "B. Brown", p1wb);
        Patient p2 = new Patient("Jared Mac", "12/22/1991", "B. Brown", p2wb);
        Patient p3 = new Patient("Muhammad Belland", "11/02/1973", "A. Lielmanis", p3wb);
        Patient p4 = new Patient("Leon Kennedy", "06/14/2000", "C. Dredon", p4wb);
        Patient p5 = new Patient("Aman Amor", "02/24/1980", "H. Proafee", p5wb);
        Patient p6 = new Patient("Carly Jean", "09/11/1967", "H. Proafee", p6wb);
        Patient p7 = new Patient("Tim Dewsberry", "09/24/1989", "A. Lielmanis", p7wb);
        Patient p8 = new Patient("Gina Dimascio", "10/05/2001", "G. Rudal", p8wb);
        Patient p9 = new Patient("Nea Carlson", "10/05/2001", "G. Rudal", p9wb);
        Patient p10 = new Patient("Asmabanu Saiyed", "12/15/2001", "K. Krisp", p10wb);
        
        //Put patients in a list
        List<Patient> patients = new ArrayList<>();
        patients.add(p1);
        patients.add(p2);
        patients.add(p3);
        patients.add(p4);
        patients.add(p5);
        patients.add(p6);
        patients.add(p7);
        patients.add(p8);
        patients.add(p9);
        patients.add(p10);
        
        
        
        //Making ResearchGroup to add list of patients to        
        ResearchGroup researchGroup1 = new ResearchGroup(patients);
       
        List<Patient> ptnt = researchGroup1.getListOfPatients();
        for(Patient ptn : ptnt){
                System.out.println("--Patient--\n" + ptn.getPatientInfo());
        }
        
        
        
    }
    
}
