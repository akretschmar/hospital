
package hospital;

import java.util.List;

public class ResearchGroup {
    private List<Patient> patients;
    
    public ResearchGroup (List<Patient> patients){
        this.patients = patients;
    }
    
    public List<Patient> getListOfPatients(){
        return patients;
    }

    
}


