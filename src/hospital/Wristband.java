
package hospital;


public class Wristband {
    private String barcode;
    private String information;
    
    public Wristband(String barcode, String information){
        this.barcode = barcode;
        this.information = information;
    }
    
    public String getBarcode(){
        return this.barcode;
    }
    
    public String getInformation(){
        return this.information;
    }
    
    @Override
    public String toString(){
        return this.barcode + " " + this.information;
    }
}
